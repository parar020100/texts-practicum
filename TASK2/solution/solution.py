from typing import List, Tuple, Dict, Set
import re
import json
from date_regex import *


####################################################################################################
# Параметры запуска

enable_training = True

debug_regex 				= False 	#or True
calculate_results 			= True 		#and False
debug_progress 				= False 	#or True
debug_results 				= False 	#or True

# Параметры построения
repeat_only_13				= False 	or True

# Даты
date_enable					= True 		#and False

date_use_regex 				= False 	or True
date_use_train_corpus 		= False 	#or True
date_split_train_corpus 	= False 	#or True

date_repeat_example_regex 	= False 	#or True


# Имена
name_enable					= True 		#and False

name_use_regex 				= False 	or True
name_use_train_corpus 		= False 	#or True
name_split_train_corpus 	= False 	#or True
name_use_wiki 				= False 	#or True
name_use_wiki_full 			= False 	#or True

name_unite 					= False 	#or True
name_repeat_example_regex 	= False 	#or True


# Организации
orgs_enable					= True 		#and False

orgs_use_regex 				= False 	or True #
orgs_use_train_corpus 		= False 	or True #
orgs_split_train_corpus 	= False 	#or True
orgs_use_wiki 				= False 	#or True
orgs_use_wiki_full 			= False 	or True #

orgs_unite 					= False 	#or True
orgs_repeat_example_regex 	= False 	or True #


# Файлы
wiki_name_file 				= "wiki_names_split.json"
wiki_name_file_full 		= "wiki_names_full.json"

wiki_orgs_file 				= "wiki_orgs_split.json"
wiki_orgs_file_full 		= "wiki_orgs_full.json"


# Уточнение

name_use_regex 				= name_enable and name_use_regex
name_use_train_corpus 		= name_enable and name_use_train_corpus
name_split_train_corpus 	= name_enable and name_split_train_corpus
name_use_wiki 				= name_enable and name_use_wiki
name_use_wiki_full 			= name_enable and name_use_wiki_full
name_unite 					= name_enable and name_unite

orgs_use_regex 				= orgs_enable and orgs_use_regex
orgs_use_train_corpus 		= orgs_enable and orgs_use_train_corpus
orgs_split_train_corpus 	= orgs_enable and orgs_split_train_corpus
orgs_use_wiki 				= orgs_enable and orgs_use_wiki
orgs_use_wiki_full 			= orgs_enable and orgs_use_wiki_full
orgs_unite 					= orgs_enable and orgs_unite


####################################################################################################
# Вспомогательные функции

regexp_letters_numbers = r'[^a-zA-Zа-яА-ЯЁё\d\s\-\']'
regexp_letters_no_brackets = r'[\(\)\.\*+\[\]]'

def debug_print_progress(text):
	if debug_progress:
		print("PROGRESS: " + text, flush=True)

def check_and_add_example(current_set, text, filter_regex):
	text = re.sub(regexp_letters_no_brackets, "", text)
	if not filter_regex.fullmatch(text) and len(text) > 4:
		if not re.fullmatch(r'[\d,]*', text):
			current_set.add(text)

def add_example(current_set, text, filter_regex, should_split):
	text_cut = re.sub(regexp_letters_numbers, "", text)

	check_and_add_example(current_set, text, filter_regex)
	if should_split:
		for word in text.split():
			check_and_add_example(current_set, word, filter_regex)

	if text_cut != text:
		check_and_add_example(current_set, text_cut, filter_regex)
		if should_split:
			for word in text_cut.split():
				check_and_add_example(current_set, word, filter_regex)

def len_sort_list(something):
	return sorted(list(something), key=len, reverse=True)

def set_to_one_regex(example_list):
	regex = '(?:'
	sorted = len_sort_list(example_list)
	for example in sorted:
		regex += example + '|'
	regex = regex.rstrip('|') + ')'
	return regex

def set_to_rep_regex(example_list):
	single = set_to_one_regex(example_list)
	if repeat_only_13:
		rpt = r'{1,3}'
	else:
		rpt = r'+'
	#final = rf'(?:(?:{single}\s?){rpt})'
	final = rf'(?:{single})|(?:(?:{single}\s?){rpt})'
	return final

def set_to_regex(example_list, rep):
	if rep:
		return set_to_rep_regex(example_list)
	return set_to_one_regex(example_list)

def read_json_sorted(file):
	with open(file, "r") as json_data:
		return sorted( list(json.load(json_data)), key=len, reverse=True)

def json_to_checked_set(file, regex):
	all_set = set()
	raw = read_json_sorted(file)
	for name in raw:
		check_and_add_example(all_set, name, regex)
	return all_set

def process_regex(text, results, regex, label):
	if regex == None:
		return
	compiled = re.compile(regex)
	for m in compiled.finditer(text):
		results.add((m.start(), m.end(), label))

####################################################################################################
# Основной класс

class Solution:

	################################################################################################
	# Изначальные регулярные выражения

	def __init__(self):
		self.date_regex_list = list()

		if date_enable:
			self.initial_date_regex = date_regex.answer
			self.initial_date_compiled = re.compile(date_regex.answer)

			if date_use_regex:
				self.date_regex_list.append(self.initial_date_regex)

		# Имена
		self.name_regex_list = list()

		if name_enable:
			rpt02 = r'{0,2}'
			n_normal = r'(?:[А-ЯЁ][а-яё]+)'
			n_initial = r'(?:[А-ЯЁ]\.)'
			n_initials = rf'(?:\s?{n_initial}\s?){rpt02}'
			name_regex = rf'(?<![\.!\?]\s)(?<=\s){n_initials}{n_normal}(?:\s?{n_initial}|\s{n_normal}){rpt02}(?=[\s\.,!\?])'

			self.initial_name_regex = name_regex
			self.initial_name_compiled = re.compile(name_regex)
			if name_use_regex:
				self.name_regex_list.append(self.initial_name_regex)

		# Организации
		self.orgs_regex_list = list()

		if orgs_enable:
			orgs_regex = r'(?:[А-ЯЁA-Z]{2,}[а-яёa-z\d]*)(?=[\s\n\.,!\?])'

			self.initial_orgs_regex = orgs_regex
			self.initial_orgs_compiled = re.compile(orgs_regex)
			if orgs_use_regex:
				self.orgs_regex_list.append(self.initial_orgs_regex)

	################################################################################################
	# Обучение

	def train(self, train_corpus: List[Tuple[str, Dict[str, Set[Tuple[int, int, str]] ]]]):

		if enable_training != True:
			return

		############################################################################################
		# Обработка обученных данных

		date_set = set()
		name_set = set()
		orgs_set = set()
		wiki_names = set()
		wiki_orgs = set()
		wiki_names_full = set()
		wiki_orgs_full = set()

		parse_text_no = 0
		if date_use_train_corpus or name_use_train_corpus or orgs_use_train_corpus:
			for entry in train_corpus:
				parse_text_no += 1
				debug_print_progress("Parsing text no: " + str(parse_text_no))

				text = entry[0]
				user_layouts = entry[1]

				entities = set()

				for key in user_layouts:
					entities.update(user_layouts[key])

				for entity in entities:
					selected = text[ entity[0] : entity[1] ]
					label = entity[2]

					if date_use_train_corpus and (label == "DATE"):
						add_example(date_set, selected, self.initial_date_compiled, date_split_train_corpus)
					if name_use_train_corpus and (label == "PERSON"):
						add_example(name_set, selected, self.initial_name_compiled, name_split_train_corpus)
					if orgs_use_train_corpus and (label == "ORGANIZATION"):
						add_example(orgs_set, selected, self.initial_orgs_compiled, orgs_split_train_corpus)

		############################################################################################
		# Обработка данных из википедии

		if name_use_wiki:
			debug_print_progress("Creating set from wiki names")
			wiki_names = json_to_checked_set(wiki_name_file, self.initial_name_compiled)

		if name_use_wiki_full:
			debug_print_progress("Creating set from wiki full names")
			wiki_names_full = json_to_checked_set(wiki_name_file_full, self.initial_name_compiled)

		if orgs_use_wiki:
			debug_print_progress("Creating set from wiki orgs")
			wiki_orgs = json_to_checked_set(wiki_orgs_file, self.initial_orgs_compiled)

		if orgs_use_wiki:
			debug_print_progress("Creating set from wiki full orgs")
			wiki_orgs_full = json_to_checked_set(wiki_orgs_file_full, self.initial_orgs_compiled)

		############################################################################################
		# Добавление регулярных выражений

		debug_print_progress("Creating regular expressions")

		if date_use_train_corpus:
			debug_print_progress("Creating regex from date examples")
			date_example_regex = set_to_regex(date_set, date_repeat_example_regex)
			self.date_regex_list.append(date_example_regex)

		if name_unite:
			debug_print_progress("Creating united regex from name examples")
			name_set_united = set()
			name_set_united.update(name_set)
			name_set_united.update(wiki_names)
			name_set_united.update(wiki_names_full)
			if len(name_set_united) > 0:
				name_united_regex = set_to_regex(name_set_united, name_repeat_example_regex)
				self.name_regex_list.append(name_united_regex)
		else:
			if name_use_train_corpus and len(name_set) > 0:
				debug_print_progress("Creating regex from name train corpus examples")
				name_example_regex = set_to_regex(name_set, name_repeat_example_regex)
				self.name_regex_list.append(name_example_regex)
			if name_use_wiki and len(wiki_names) > 0:
				debug_print_progress("Creating regex from name wiki examples")
				name_wiki_regex = set_to_regex(wiki_names, name_repeat_example_regex)
				self.name_regex_list.append(name_wiki_regex)
			if name_use_wiki_full and len(wiki_names_full) > 0:
				debug_print_progress("Creating regex from name wiki full examples")
				name_wiki_regex_full = set_to_regex(wiki_names_full, name_repeat_example_regex)
				self.name_regex_list.append(name_wiki_regex_full)

		if orgs_unite:
			debug_print_progress("Creating united regex from org examples")
			orgs_set_united = set()
			orgs_set_united.update(orgs_set)
			orgs_set_united.update(wiki_orgs)
			orgs_set_united.update(wiki_orgs_full)
			if len(orgs_set_united) > 0:
				orgs_united_regex = set_to_regex(orgs_set_united, orgs_repeat_example_regex)
				self.orgs_regex_list.append(orgs_united_regex)
		else:
			if orgs_use_train_corpus and len(orgs_set) > 0:
				debug_print_progress("Creating regex from org train corpus examples")
				orgs_example_regex = set_to_regex(orgs_set, orgs_repeat_example_regex)
				self.orgs_regex_list.append(orgs_example_regex)
			if orgs_use_wiki and len(wiki_orgs) > 0:
				debug_print_progress("Creating regex from org wiki examples")
				orgs_wiki_regex = set_to_regex(wiki_orgs, orgs_repeat_example_regex)
				self.orgs_regex_list.append(orgs_wiki_regex)
			if orgs_use_wiki_full and len(wiki_orgs_full) > 0:
				debug_print_progress("Creating regex from org wiki full examples")
				orgs_wiki_regex_full = set_to_regex(wiki_orgs_full, orgs_repeat_example_regex)
				self.orgs_regex_list.append(orgs_wiki_regex_full)

	################################################################################################
	# Получение ответа

	def print_all_regexes(self):
		if date_enable:
			i = 0
			for regex in self.date_regex_list:
				i += 1
				print("\nDATE " + str(i) + ": " + regex)

		if name_enable:
			i = 0
			for regex in self.name_regex_list:
				i += 1
				print("\nPERSON " + str(i) + ": " + regex)

		if orgs_enable:
			i = 0
			for regex in self.orgs_regex_list:
				i += 1
				print("\nORGANIZATION " + str(i) + ": " + regex)

		print("\n")
		return


	def predict(self, news: List[str]) -> List[Set[Tuple[int, int, str]]]:
		if debug_regex:
			self.print_all_regexes()

		if not calculate_results:
			return

		debug_print_progress("Predicting texts")

		res = []
		text_no = 0
		for text in news:
			text_no += 1
			debug_print_progress("Predicting text no: " + str(text_no))

			s = set()

			if date_enable:
				for regex in self.date_regex_list:
					process_regex(text, s, regex, 'DATE')

			if name_enable:
				for regex in self.name_regex_list:
					process_regex(text, s, regex, 'PERSON')

			if orgs_enable:
				for regex in self.orgs_regex_list:
					process_regex(text, s, regex, 'ORGANIZATION')

			if debug_results:
				for entry in s:
					selected = text[ entry[0] : entry[1] ]
					print(entry[2] + " " + selected)

			res.append(s)
		return res



