from requests import Session
import time
import json
import re
from date_regex import *

def wiki_download_list(category, full_file, split_file):

	URL = "https://ru.wikipedia.org/w/api.php"
	PARAMS = {
		"action": "query",
		"format": "json",
		"list": "categorymembers",
		"utf8": 1,
		"cmtitle": category,
		"cmprop": "title",
		"cmnamespace": "0",
		"cmtype": "page",
		"cmcontinue": "-||",
		"cmlimit": "3000"
	}

	session = Session()

	titles = set()
	words = set()

	start = time.time()
	iters = 0
	total = 500_000

	regexp_letters_numbers = r'[^a-zA-Zа-яА-ЯЁё\d\s\-\']'

	while True:
		response = session.get(url = URL, params = PARAMS)
		data = response.json()
		pages = data['query']['categorymembers']

		for page in pages:
			title = page['title']
			title_acc_symb = re.sub(regexp_letters_numbers, "", title)

			titles.add(title_acc_symb)

			title_split_words = title_acc_symb.split()
			for word in title_split_words:
				word_no_date = re.sub(date_regex.compiled, "", word)
				if re.match(r'^[\d]*(?:я|е|й|го|его)?$', word_no_date) is None:
					if len(word_no_date) > 2:
						words.add(word_no_date)

		iters += 1

		#if iters > 10:
			#break

		if iters % 10 == 0:
			cur = time.time()
			passed = cur - start
			avg = passed / iters
			left = total - iters * 500
			time_left = left / 500 * avg

			print('Results got:', iters * 500)
			print('Results left:', left)
			print('Time passed:', str(int(passed)) + ' seconds')
			print('Time left (estimated):', str(int(time_left)) + ' seconds')

		cmcontinue = data.get('continue')
		if cmcontinue is None:
			break
		PARAMS['cmcontinue'] = cmcontinue['cmcontinue']

	#file = open('wiki_names.json', 'w')

	with open(full_file, "w") as titles_file:
		json.dump(sorted(list(titles)), titles_file, ensure_ascii=False, indent=2)

	with open(split_file, "w") as words_file:
		json.dump(sorted(list(words)), words_file, ensure_ascii=False, indent=2)


