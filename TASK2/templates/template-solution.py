from typing import List, Tuple, Dict, Set

class Solution:
	def train(self, train_corpus: List[Tuple[str, Dict[str, Set[Tuple[int, int, str]]]]]):
		pass

	def predict(self, news: List[str]) -> List[Set[Tuple[int, int, str]]]:
		return [set() for _ in news]