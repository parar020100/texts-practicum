from typing import List, Tuple, Dict, Set
import re

class Solution:

    def __init__(self):
        self.date_regex = re.compile(myregex.DATE_REGEXP_FULL) 

    def train(self, train_corpus: List[Tuple[str, Dict[str, Set[Tuple[int, int, str]]]]]):
        pass

    DATE_REGEXP = r''

    reg = re.compile(DATE_REGEXP)

    def predict(self, news: List[str]) -> List[Set[Tuple[int, int, str]]]:
        res = []
        for text in news:
            s = set()
            for m in self.reg.finditer(text):
                s.add((m.start(), m.end(), 'DATE'))
            res.append(s)
        return res
