#! /bin/python3


from solution import *
import json
from typing import Dict
#from utils import files

def translate_dict(dictionary: Dict):
    if len(dictionary) == 3:
        return dictionary['start'], dictionary['end'], dictionary['label']
    else:
        return dictionary


# прочитать json
file = open("_tpc-dataset.train.json", "r")
print("LOG: loading dataset")
dataset = json.load(file, object_hook=translate_dict)

formatted_dataset = dict()

# отформатировать в нужный формат
print("LOG: formatting dataset")
for entry in dataset:
    if entry['questionId'] in formatted_dataset:
        formatted_dataset[entry['questionId']][1][entry['userId']] = set(entry['entities'])
    else:
        formatted_dataset[entry['questionId']] = (entry['text'], {entry['userId']: set(entry['entities'])})

# поделить на тренировочною и тестовую выборки
print("LOG: splitting dataset")
train_size = round(len(formatted_dataset) * 0.75)
train_dataset = list(formatted_dataset.values())[0:train_size]
check_dataset = list(formatted_dataset.values())[train_size:]

#train_dataset = list(formatted_dataset.values())[0:100]
#check_dataset = list(formatted_dataset.values())[101:200]

check_text = list(map(lambda x: x[0], check_dataset))
check_results = list(map(lambda x: list(x[1].values())[0], check_dataset))

# обучить
print("LOG: training solution")
solution = Solution()
solution.train(train_corpus=train_dataset)

# получить результат на тестах
print("LOG: predicting results")
result = solution.predict(check_text)

print("\n")
print(result)

exit()
# посчитать качество
#quality = QualityNERC()
#quality.evaluate(result, check_results, {"PERSON", "ORGANIZATION", "DATE"}, debug=True)
print(result)
