#! /bin/python3

def a(b, c):
	if c:
		print(b)
	else:
		print("A" + b)

a("Z", "E")
a("Z", None)

print(1 == 1)
