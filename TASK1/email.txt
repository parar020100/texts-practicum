Good:
somename@example.com
some.name@example.com
somename1@example.com
"Xyz@func"@example.com
"John Men"@example.com
order/action=social@example.com
" "@example.com
example@com
$S4372@example.com
!xyz!func%qwe@example.com
some.name@[127.0.0.1]
somename@[127.0.0.1]

Bad:
example
#%@#^$#@#$@#.com
exam..ple@com
.exam.ple@com
Xyz\@func@example.com
email@1.22.333.4444.
email@-example.com
$S4372@.example.com
just\"not\"right@example.com


Good:
проверка somename@example.com я пишу email@ на адрес some.name@example.com, потому что адрес somename1@example.com не совсем соответсвует цели: "Xyz@func"@example.com
А вот ещё пример: "John Men"@example.com, достаточно прикольный по моим меркам.
Адрес order/action=social@example.com, а ещё " "@example.com example@com $S4372@example.com !xyz!func%qwe@example.com some.name@[127.0.0.1] somename@[127.0.0.1]

Bad:
example дфдфдфдф #%@#^$#@#$@#.com exam..ple@com .exam.ple@com Xyz\@func@example.com email@1.22.333.4444. email@-example.com  $S4372@.example.com, а just\"not\"right@example.com



Точки: (?!^\..*$)(?!^.*\.$)(?!.*\.\..*)
Символы без кавычек: [a-zA-z\d!#$%&'*+\-\/=?^_`{|}~]
Символы с кавычками: [a-zA-z\d!#$%&'*+\-\/=?^_`{|}~\"(),:;<>@\[\\\]]
Хост: [a-z\d\-]

Без кавычек: (?!^\..*$)(?!^.*\.$)(?!.*\.\..*)[a-zA-z\d!#$%&'*+\-\/=?^_`{|}~]
С кавычками: [a-zA-z\d!#$%&'*+\-\/=?^_`{|}~\"(),:;<>@\[\\\]]

Каркас: ^(?:|\"*\")*@[^\s]*$


^(?:(?!^\..*$)(?!^.*\.$)(?!.*\.\..*)[\.a-zA-z\d!#$%&'*+\-\/=?^_`{|}~]*|\"[ a-zA-z\d!#$%&'*+\-\/=?^_`{|}~\"(),:;<>@\[\\\]]*\")@(?:[a-z\d]+(?:|[\.a-z\d\-]*[a-z\d])|\[[0-9\.]*\])$

/^(?:(?!^\..*$)(?!^.*\.$)(?!.*\.\..*)[\.a-zA-z\d!#$%&'*+\-\/=?^_`{|}~]*|\"[ a-zA-z\d!#$%&'*+\-\/=?^_`{|}~\"(),:;<>@\[\\\]]*\")@(?:[a-z\d]+(?:|[\.a-z\d\-]*[a-z\d])|\[[0-9\.]*\])$/gm

