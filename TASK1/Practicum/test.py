import re
from solution import *
from compile import *

print(URI.findall("smb://server/dirpath/file.name \
smb://smedley/?NBNS=172.24.19.18;NODETYPE=H \
smb://corgis/?NODETYPE=B \
smb://jcifs.samba.org/?NODETYPE=;CALLED=SMBSERV \
smb://bran/SCOPE= \
smb://marika/SCOPE=;NODETYPE=B \
smb://jcifs/?CALLED=VIRTSERV;NBNS=172.24.19.18 \
smb://corgis/docs/ \
smb://corgis/docs/jolyon/ \
smb://corgis/docs/jolyon/rabbit.txt \
ftp://ftp.is.co.za/rfc/rfc1808.txt \
http://www.ietf.org/rfc/rfc2396.txt \
ftp://cnn.example.com&story=breaking_news@10.0.0.1/top_story.htm \
ftp://foo.example.com/rfc/ \
ssh://user;fingerprint=ssh-dss-c1-b1-30-29-d7-b8-de-6c-97-77-10-d7-46-41-63-87@host.example.com \
ssh://user@host.example.com:2222 \
ssh://user@host.example.com \
sftp://user@host.example.com/~/file.txt \
sftp://user@host.example.com/dir/path/file.txt \
sftp://user;fingerprint=ssh-dss-c1-b1-30-29-d7-b8-de-6c-97-77-10-d7-46-41-63-87@host.example.com:2222/;type=d \
http://127.0.0.1:1235 \
smb://192.168.1.7/USER$/ \
ftp://cnn.example.com&story=breaking_news@10.0.0.1/top_story.htm \
( ftp://ftp.is.co.za/rfc/rfc1808.txt)"))