import re
from solution import *

PASSWORD = re.compile(PASSWORD_REGEXP)
EMAIL = re.compile(EMAIL_REGEXP)
URI = re.compile(URI_REGEXP)
DATE = re.compile(DATE_REGEXP)
